

function MainPage() {
  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="title display-5 fw-bold">M && M</h1>
      <h2 className="title display-5 fw-bold">Automobiles</h2>
      <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
          The Best of German Ingenuity Guaranteed!
        </p>
      </div>
      <div id="carouselExampleCaptions" className="carousel slide" data-bs-ride="carousel">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></button>
  </div>
  <div className="carousel-inner">
    <div className="carousel-item active">
      <img src="https://cdn.carbuzz.com/gallery-images/1600/1028000/0/1028060.jpg" className="d-block w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <h5 className="slide-label">Mercedes-Benz AMG SL-63</h5>
        <p className="description-text">$174,999.00 · 2-Door · AMG 4.0L V8 BiTurbo </p>
      </div>
    </div>
    <div className="carousel-item">
      <img src="https://i.pinimg.com/736x/40/56/92/40569293d665e0c46c1308f0652d551e.jpg" className="d-block w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <h5 className="slide-label">Audi R8</h5>
        <p className="description-text">$164,999.00 · 2-Door · 5.2L V10</p>
      </div>
    </div>
    <div className="carousel-item">
      <img src="https://car-images.bauersecure.com/wp-images/4179/bmwm3_108.jpg" className="d-block w-100" alt="..."/>
      <div className="carousel-caption d-none d-md-block">
        <h5 className="slide-label">BMW M3 Competition xDrive</h5>
        <p className="description-text">$89,999.00 · 4-Door · inline-6 TwinTurbo</p>
      </div>
    </div>
  </div>
  <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Previous</span>
  </button>
  <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
    <span className="carousel-control-next-icon" aria-hidden="true"></span>
    <span className="visually-hidden">Next</span>
  </button>
</div>
    </div>
  );
}

export default MainPage;
