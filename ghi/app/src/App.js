import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './ServiceComponents/TechnicianForm';
import ServiceAppointmentForm from './ServiceComponents/ServiceAppointmentForm';
import SalesForm from './SalesComponents/SalesForm';
import SalesList from './SalesComponents/SalesList';
import SalesPersonForm from './SalesComponents/SalesPersonForm';
import CustomerForm from './SalesComponents/CustomerForm';
import SalesHistory from './SalesComponents/SalesHistory';
import BMWList from './InventoryComponents/BMWList';
import ManufacturerList from './InventoryComponents/ManufacturerList';
import AutomobileList from './InventoryComponents/AutomobileList';
import ManufacturerForm from './InventoryComponents/ManufacturerForm';
import VehicleModelForm from './InventoryComponents/VehicleModelForm';
import AutomobileForm from './InventoryComponents/AutomobileForm';
import ServiceAppointmentList from './ServiceComponents/ServiceAppointmentList';
import ServiceHistory from './ServiceComponents/ServiceHistory';
import MercedesList from './InventoryComponents/MercedesList';
import AudiList from './InventoryComponents/AudiList';
import VehicleModelsList from './InventoryComponents/VehicleModelsList';
import EmployeesList from './Employees';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route path="/" element={<MainPage />} />

          <Route path="employee/">
            <Route path="list/" element={<EmployeesList />} />
            <Route path="technician/" element={<TechnicianForm />} />
            <Route path="salesperson/" element={<SalesPersonForm />} />
          </Route>

          <Route path="sales/">
            <Route path="create/" element={<SalesForm />} />
            <Route path="list/" element={<SalesList />} />
            <Route path="history/" element={<SalesHistory />} />
          </Route>

          <Route path="models/">
            <Route path="list/" element={<VehicleModelsList />} />
            <Route path="Audi/" element={<AudiList />} />
            <Route path="BMW/" element={<BMWList />} />
            <Route path="Mercedes-Benz/" element={<MercedesList />} />
            <Route path="create/" element={<VehicleModelForm />} />
          </Route>

          <Route path="manufacturers/">
            <Route path="list/" element={<ManufacturerList />} />
            <Route path="create/" element={<ManufacturerForm />} />
          </Route>

          <Route path="inventory/">
            <Route path="list/" element={<AutomobileList />} />
            <Route path="create/" element={<AutomobileForm />} />
          </Route>

          <Route path="appointments/">
            <Route path="list/" element={<ServiceAppointmentList />} />
            <Route path="history/" element={<ServiceHistory />} />
            <Route path="create/" element={<ServiceAppointmentForm />} />
          </Route>

          <Route path="customers/create/" element={<CustomerForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
