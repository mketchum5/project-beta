import React from "react";

class ServiceHistory extends React.Component {

        constructor(props) {
            super(props)
            this.state = {
                appointments: [],
                new_vin: "",
                appointment_results: [],
            };
            this.handleInput = this.handleInput.bind(this)
            this.refresh = this.refresh.bind(this);
        }
        async refresh() {
            const appointmentDataURL = "http://localhost:8080/api/appointments/"

            const response = await fetch(appointmentDataURL);

            if (response.ok) {
                const data = await response.json();
                if (this.state.salesperson === '') {
                    this.setState({
                        appointments: data.appointments,
                    })
                }
                else {
                let results = this.state.appointments.filter(appt => appt.new_vin.includes(this.state.new_vin))

                this.setState({
                    appointments: data.appointments,
                    appointment_results: results,
                    })
                }
            };
        }
        async componentDidMount() {
            this.refresh()
        }
        async handleInput(event) {
            const {id, value} = event.target;
            this.setState({[id]: value.toUpperCase()}, () => {
                this.refresh()
            });
        }
        render() {
            return (

                <div className="container">
                    <div>
                        <h1 className="header-title">Enter a VIN</h1>
                        <form onSubmit={this.refresh}>
                        <input
                            className="form-control" onChange={this.handleInput}
                            id="new_vin" name={this.state.new_vin} maxLength={17}
                            minLength={17} required type="text" placeholder="Enter the VIN"/>
                        </form>
                    </div>
                    <br/>
                <table className="table">
                    <thead>
                        <tr>
                            <th className="table-head"> VIN </th>
                            <th className="table-head"> Customer Name </th>
                            <th className="table-head"> Date </th>
                            <th className="table-head"> Time </th>
                            <th className="table-head"> Reason </th>
                            <th className="table-head"> Technician </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.appointment_results.map(appointment => {
                            return (
                                <tr key={appointment.id}>
                                    <td className="model-text">{appointment.new_vin}</td>
                                    <td className="model-text">{appointment.name}</td>
                                    <td className="model-text">{appointment.date}</td>
                                    <td className="model-text">{appointment.time.slice(0, -3) }</td>
                                    <td className="model-text">{appointment.reason}</td>
                                    <td className="model-text">{appointment.technician.name}</td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
                </div>
            )
        }
}


export default ServiceHistory;
