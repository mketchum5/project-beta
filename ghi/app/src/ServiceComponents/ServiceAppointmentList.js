import React from "react";

class ServiceAppointmentList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            appointments: [],
        };

        this.cancel = this.cancel.bind(this);
        this.finished = this.finished.bind(this);
        this.vipStatusImageChange = this.vipStatusImageChange.bind(this);
    }


    async componentDidMount() {
        const appointmentDataURL = "http://localhost:8080/api/appointments/"
        const response = await fetch(appointmentDataURL)

        if (response.ok) {
            const data = await response.json();
            const unfinishedAppointments = data.appointments.filter(
            (appointment) => appointment.finished === false
            );
            this.setState({appointments: unfinishedAppointments})
        }
    }

    async cancel(event) {
        const url = `http://localhost:8080/api/appointments/${event}/`
        const fetchConfig = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json"
            }
        }

        await fetch(url, fetchConfig)
        this.componentDidMount()
    }

    async finished(event) {
        const url = `http://localhost:8080/api/appointments/${event}/`
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ finished: true }),
            headers: {
                "Content-Type": "application/json"
            }
        }
        await fetch(url, fetchConfig)
        this.componentDidMount()
    }


    vipStatusImageChange(vip) {
        if (vip === true) {
            return <img className="serviceImg" src="https://i.postimg.cc/66vyK1Fq/vip-sticker-trans.png" alt='' />
        }
        return <img src="" alt='' />
    }

    render() {
        return (
            <div className="container">
                <div>
                <h1 className="header-title">Appointment List</h1>
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <th className="table-head"> VIN </th>
                        <th className="table-head"> Customer Name </th>
                        <th className="table-head"> Date </th>
                        <th className="table-head"> Time </th>
                        <th className="table-head"> Reason </th>
                        <th className="table-head"> Technician </th>
                        <th className="table-head">VIP Treatment</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.appointments.map(appointment => {
                        return (
                            <tr key={appointment.id}>
                                <td className="model-text">{appointment.new_vin}</td>
                                <td className="model-text">{appointment.name}</td>
                                <td className="model-text">{appointment.date}</td>
                                <td className="model-text">{appointment.time}</td>
                                <td className="model-text">{appointment.reason}</td>
                                <td className="model-text">{appointment.technician.name}</td>
                                <td>{this.vipStatusImageChange(appointment.vip)}</td>
                                <td>
                            <button button="true" type="button" className="btn btn-danger" onClick={() => this.cancel(appointment.id)}>Cancel</button>
                            </td>
                            <td>
                            <button button="true" type="button" className="btn btn-success" onClick={() => this.finished(appointment.id)}>Finished</button>
                            </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            </div>
        )
    }



}


export default ServiceAppointmentList;
