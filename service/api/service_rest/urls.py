from django.urls import path
from .views import api_technicians, api_appointment, api_appointments, api_automobileVOs, api_technician



urlpatterns = [
    path("technician/", api_technicians, name="api_technicians"),
    path("technician/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>/", api_appointment, name="api_appointment"),
    path("automobiles/", api_automobileVOs, name="api_automobiles"),
    ]
