from common.json import ModelEncoder
from .models import AutomobileVO, ServiceAppointment, Technician

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_id",
    ]


class ServiceAppointmentEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "id",
        "new_vin",
        "name",
        "date",
        "time",
        "reason",
        "technician",
        "finished",
    ]
    encoders = {"technician": TechnicianEncoder(),}

    def get_extra_data(self, o):
        try:
            AutomobileVO.objects.get(vin=o.new_vin)
            return {"vip": True}
        except:
            return {"vip": False}
