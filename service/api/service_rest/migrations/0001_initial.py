# Generated by Django 4.0.3 on 2022-10-25 01:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AutomobileVO',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.CharField(max_length=17, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Technician',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, null=True)),
                ('employee_id', models.PositiveSmallIntegerField(unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='ServiceAppointment',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vin', models.CharField(max_length=17, unique=True)),
                ('name', models.CharField(max_length=200)),
                ('date', models.DateField(null=True)),
                ('time', models.TimeField(null=True)),
                ('vip', models.BooleanField(default=False)),
                ('finished', models.BooleanField(default=False)),
                ('technician', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='appointment', to='service_rest.technician')),
            ],
        ),
    ]
