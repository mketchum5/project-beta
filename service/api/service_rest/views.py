from django.shortcuts import render
import json
from django.http import JsonResponse
from .models import AutomobileVO, Technician, ServiceAppointment
from .encoders import TechnicianEncoder, ServiceAppointmentEncoder, AutomobileVOEncoder
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )

    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
                )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            Technician.objects.filter(id=pk).update(**content)
            appointment = Technician.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = ServiceAppointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=ServiceAppointmentEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(employee_id=content["technician"])
            content["technician"] = technician
            appointment = ServiceAppointment.objects.create(**content)

            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )

        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician entered does not exist"})
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET", "PUT"])
def api_appointment(request, pk):
    if request.method == "GET":
        try:
            model = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                model,
                encoder=ServiceAppointmentEncoder,
                safe=False
            )
        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            model = ServiceAppointment.objects.get(id=pk)
            model.delete()
            return JsonResponse(
                model,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )
        except ServiceAppointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"})
    else:
        try:
            content = json.loads(request.body)
            ServiceAppointment.objects.filter(id=pk).update(**content)
            appointment = ServiceAppointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=ServiceAppointmentEncoder,
                safe=False,
            )

        except ServiceAppointment.DoesNotExist:
            response = JsonResponse({"message": "Appointment does not exist"})
            response.status_code = 404
            return response

@require_http_methods(["GET"])
def api_automobileVOs(request):
    automobiles = AutomobileVO.objects.all()
    return JsonResponse({"automobiles": automobiles}, encoder=AutomobileVOEncoder)

@require_http_methods(["DELETE"])
def api_automobileVO(pk):
    try:
        vin = AutomobileVO.objects.get(id=pk)
        vin.delete()
        return JsonResponse(
            vin,
            encoder=AutomobileVOEncoder,
            safe=False,
        )
    except AutomobileVO.DoesNotExist:
        return JsonResponse({"message": "Does not exist"})
